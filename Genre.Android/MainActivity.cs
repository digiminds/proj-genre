﻿using System.Globalization;
using Android.App;
using Android.Content.PM;
using Android.OS;

namespace Genre.Android
{
    [Activity(Label = "Genre", Icon = "@mipmap/icon", Theme = "@style/SplashTheme", MainLauncher = true, ConfigurationChanges = ConfigChanges.ScreenSize | ConfigChanges.Orientation)]
    public class MainActivity : Xamarin.Forms.Platform.Android.FormsAppCompatActivity
    {
        private App _app;
        protected override void OnCreate(Bundle savedInstanceState)
        {
            TabLayoutResource = Resource.Layout.Tabbar;
            ToolbarResource = Resource.Layout.Toolbar;

            var h = new HijriCalendar();
            var p = new PersianCalendar();
            var u = new UmAlQuraCalendar();
            var t = new ThaiBuddhistCalendar();

            base.OnCreate(savedInstanceState);
            Xamarin.Forms.Forms.Init(this, savedInstanceState);
            _app = new App();
            LoadApplication(_app);
        }

        public override void OnBackPressed()
        {
            if (_app.DoBack)
            {
                base.OnBackPressed();
            }
        }
    }
}