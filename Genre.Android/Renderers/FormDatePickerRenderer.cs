﻿using System.ComponentModel;
using Android.Content;
using Android.Graphics;
using Genre.Android.Renderers;
using Genre.Elements;
using Xamarin.Forms;
using Xamarin.Forms.Platform.Android;
using Color = Xamarin.Forms.Color;
using DatePicker = Xamarin.Forms.DatePicker;

[assembly: ExportRenderer(typeof(FormDatePicker), typeof(FormDatePickerRenderer))]
namespace Genre.Android.Renderers
{
    public class FormDatePickerRenderer : DatePickerRenderer
    {
        FormDatePicker element;
        public FormDatePickerRenderer(Context context) : base(context)
        {
        }

        protected override void OnElementChanged(ElementChangedEventArgs<DatePicker> e)
        {
            base.OnElementChanged(e);

            if (e.OldElement != null || e.NewElement == null)
                return;

            element = (FormDatePicker) Element;
            element.SetLineColor = SetLineColor;

            if (!string.IsNullOrEmpty(element.Placeholder))
            {
                Control.Text = element.Required ? $"{element.Placeholder} *" : element.Placeholder;
            }

            SetLineColor();
        }

        protected override void OnElementPropertyChanged(object sender, PropertyChangedEventArgs e)
        {
            base.OnElementPropertyChanged(sender, e);

            if (e.PropertyName.Equals("IsFocused"))
            {
                var element = (FormDatePicker)Element;
                if (element.IsFocused == false)
                {
                    element.IsValid();
                }
            }
        }

        private bool SetLineColor(Color color = default(Color))
        {
            if (color == default(Color))
            {
                Control.Background.SetColorFilter(element.LineColor.ToAndroid(), PorterDuff.Mode.SrcAtop);
            }
            else
            {
                Control.Background.SetColorFilter(color.ToAndroid(), PorterDuff.Mode.SrcAtop);
            }

            return true;
        }
    }
}