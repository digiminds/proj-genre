﻿using System.ComponentModel;
using Android.Content;
using Android.Graphics;
using Genre.Android.Renderers;
using Genre.Elements;
using Xamarin.Forms;
using Xamarin.Forms.Platform.Android;
using Color = Xamarin.Forms.Color;

[assembly: ExportRenderer(typeof(FormEditor), typeof(FormEditorRenderer))]
namespace Genre.Android.Renderers
{
    public class FormEditorRenderer : EditorRenderer
    {
        FormEditor element;
        public FormEditorRenderer(Context context) : base(context)
        {
        }

        protected override void OnElementChanged(ElementChangedEventArgs<Editor> e)
        {
            base.OnElementChanged(e);

            if (e.OldElement != null || e.NewElement == null)
                return;

            element = (FormEditor)Element;
            element.SetLineColor = SetLineColor;

            if (element.Required)
            {
                element.Placeholder = $"{element.Placeholder} *";
            }

            SetLineColor();
        }

        protected override void OnElementPropertyChanged(object sender, PropertyChangedEventArgs e)
        {
            base.OnElementPropertyChanged(sender, e);

            if (e.PropertyName.Equals("IsFocused"))
            {
                element = (FormEditor)Element;
                if (element.IsFocused == false)
                {
                    element.IsValid();
                }
            }
        }

        private bool SetLineColor(Color color = default(Color))
        {
            if (color == default(Color))
            {
                Control.Background.SetColorFilter(element.TextColor.ToAndroid(), PorterDuff.Mode.SrcAtop);
            }
            else
            {
                Control.Background.SetColorFilter(color.ToAndroid(), PorterDuff.Mode.SrcAtop);
            }

            return true;
        }
    }
}