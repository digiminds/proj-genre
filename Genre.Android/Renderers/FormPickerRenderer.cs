﻿using System;
using System.ComponentModel;
using Android.Content;
using Android.Graphics;
using Android.Graphics.Drawables;
using Android.Support.V4.Content;
using Android.Util;
using Android.Views;
using Genre.Android.Renderers;
using Genre.Elements;
using Xamarin.Forms;
using Xamarin.Forms.Platform.Android;
using Color = Xamarin.Forms.Color;

[assembly: ExportRenderer(typeof(FormPicker), typeof(FormPickerRenderer))]
namespace Genre.Android.Renderers
{
    public class FormPickerRenderer : PickerRenderer
    {
        FormPicker element;
        
        private FlowDirection _flowDirection = Device.FlowDirection;

        public FormPickerRenderer(Context context) : base(context)
        {
        }

        protected override void OnElementChanged(ElementChangedEventArgs<Picker> e)
        {
            base.OnElementChanged(e);

            if (e.NewElement != null)
            {
                element = (FormPicker)Element;
                element.SetLineColor = SetLineColor;

                if (element.IsCurvedCornersEnabled)
                {
                    // creating gradient drawable for the curved background
                    var gradientBackground = new GradientDrawable();
                    gradientBackground.SetShape(ShapeType.Rectangle);
                    gradientBackground.SetColor(element.BackgroundColor.ToAndroid());

                    // Thickness of the stroke line
                    gradientBackground.SetStroke(element.BorderWidth, element.BorderColor.ToAndroid());
                    element.BackgroundColor = Xamarin.Forms.Color.Transparent;

                    // Radius for the curves
                    gradientBackground.SetCornerRadius(
                        DpToPixels(this.Context,
                            Convert.ToSingle(element.CornerRadius)));

                    Drawable[] layers = { gradientBackground, GetDrawable(element.ImagePath) };
                    var layerDrawable = new LayerDrawable(layers);
                    layerDrawable.SetLayerInset(0, 0, 0, 0, 0);

                    // set the background of the label
                    Control.SetBackground(layerDrawable);

                    // Set padding for the internal text from border
                    Control.SetPadding(
                        (int)DpToPixels(this.Context, Convert.ToSingle(12)),
                        Control.PaddingTop,
                        (int)DpToPixels(this.Context, Convert.ToSingle(12)),
                        Control.PaddingBottom);
                }
                else
                {
                    if (_flowDirection == FlowDirection.RightToLeft)
                    {
                        Control.SetCompoundDrawablesWithIntrinsicBounds(GetDrawable("picker_icon"), null, null, null); 
                    }
                    else
                    {
                        Control.SetCompoundDrawablesWithIntrinsicBounds(null, null, GetDrawable("picker_icon"), null); 
                    }
                    Control.CompoundDrawablePadding = 25;

                    if (!string.IsNullOrEmpty(element.Placeholder))
                    {
                        Control.Text = element.Required ? $"{element.Placeholder} *" : element.Placeholder;
                    }
                    SetLineColor();
                }
            }
        }

        protected override void OnElementPropertyChanged(object sender, PropertyChangedEventArgs e)
        {
            base.OnElementPropertyChanged(sender, e);
            if (e.PropertyName.Equals("IsFocused"))
            {
                element = (FormPicker)Element;
                if (element.IsFocused == false)
                {
                    element.IsValid();
                }
            }
        }

        private bool SetLineColor(Color color = default(Color))
        {
            if (color == default(Color))
            {
                Control.Background.SetColorFilter(element.BorderColor.ToAndroid(), PorterDuff.Mode.SrcAtop);
            }
            else
            {
                Control.Background.SetColorFilter(color.ToAndroid(), PorterDuff.Mode.SrcAtop);
            }

            return true;
        }

        private static float DpToPixels(Context context, float valueInDp)
        {
            DisplayMetrics metrics = context.Resources.DisplayMetrics;
            return TypedValue.ApplyDimension(ComplexUnitType.Dip, valueInDp, metrics);
        }

        private BitmapDrawable GetDrawable(string imagePath)
        {
            if (string.IsNullOrEmpty(imagePath))
                imagePath = "picker_icon";
            if (imagePath.Contains("."))
                imagePath = imagePath.Split('.')[0];

            var resId = Resources.GetIdentifier(imagePath, "drawable", this.Context.PackageName);
            var drawable = ContextCompat.GetDrawable(this.Context, resId);
            var bitmap = ((BitmapDrawable)drawable).Bitmap;

            var result =
                new BitmapDrawable(Resources, Bitmap.CreateBitmap(bitmap))
                {
                    Gravity = _flowDirection == FlowDirection.RightToLeft ? GravityFlags.Left : GravityFlags.Right
                };

            return result;
        }
    }
}