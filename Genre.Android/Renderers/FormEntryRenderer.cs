﻿using System.ComponentModel;
using Android.Content;
using Android.Graphics;
using Android.Graphics.Drawables;
using Android.Support.V4.Content;
using Genre.Android.Renderers;
using Genre.Elements;
using Xamarin.Forms;
using Xamarin.Forms.Platform.Android;
using Color = Xamarin.Forms.Color;
using TextAlignment = Android.Views.TextAlignment;

[assembly: ExportRenderer(typeof(FormEntry), typeof(FormEntryRenderer))]
namespace Genre.Android.Renderers
{
    public class FormEntryRenderer : EntryRenderer
    {
        FormEntry element;
        public FormEntryRenderer(Context context) : base(context)
        {

        }
        protected override void OnElementChanged(ElementChangedEventArgs<Entry> e)
        {
            base.OnElementChanged(e);

            if (e.OldElement != null || e.NewElement == null)
                return;

            element = (FormEntry)Element;
            element.SetLineColor = SetLineColor;

            SetCompound();
            SetLineColor();
        }

        protected override void OnElementPropertyChanged(object sender, PropertyChangedEventArgs e)
        {
            base.OnElementPropertyChanged(sender, e);

            if (e.PropertyName.Equals("IsFocused"))
            {
                element = (FormEntry)Element;
                if (element.IsFocused == false)
                {
                    element.IsValid();
                }
            }
            else if (e.PropertyName.Equals("Text"))
            {
                if (element.Invalid)
                {
                    element.IsValid();
                }
            }
        }

        private bool SetLineColor(Color color = default(Color))
        {
            if (color == default(Color))
            {
                Control.Background.SetColorFilter(element.LineColor.ToAndroid(), PorterDuff.Mode.SrcAtop);
            }
            else
            {
                Control.Background.SetColorFilter(color.ToAndroid(), PorterDuff.Mode.SrcAtop);
            }

            return true;
        }

        private void SetCompound()
        {
            var editText = Control;
            editText.TextAlignment = TextAlignment.ViewStart;

            if (element.Required)
            {
                var flowDirection = Device.FlowDirection;
                if (flowDirection == FlowDirection.RightToLeft)
                {
                    if (element.IsPassword)
                    {
                        element.Placeholder = $"* {element.Placeholder}";
                    }
                    else
                    {
                        element.Placeholder = $"{element.Placeholder} *";
                    }
                }
                else
                {
                    element.Placeholder = $"{element.Placeholder} *";
                }
            }

            if (!string.IsNullOrEmpty(element.Image))
            {
                switch (element.ImageAlignment)
                {
                    case ImageAlignment.Left:
                        editText.SetCompoundDrawablesWithIntrinsicBounds(GetDrawable(element.Image), null, null, null);
                        break;
                    case ImageAlignment.Right:
                        editText.SetCompoundDrawablesWithIntrinsicBounds(null, null, GetDrawable(element.Image), null);
                        break;
                    case ImageAlignment.Default:
                        {
                            var flowDirection = Device.FlowDirection;
                            if (flowDirection == FlowDirection.RightToLeft)
                                goto case ImageAlignment.Right;
                            goto case ImageAlignment.Left;
                        }

                }
            }
            editText.CompoundDrawablePadding = 25;
        }

        private BitmapDrawable GetDrawable(string formEntryImage, int width = 0, int height = 0)
        {
            int resID = Resources.GetIdentifier(formEntryImage, "drawable", this.Context.PackageName);
            var drawable = ContextCompat.GetDrawable(this.Context, resID);
            var bitmap = ((BitmapDrawable)drawable).Bitmap;

            return new BitmapDrawable(Resources,
                Bitmap.CreateScaledBitmap(bitmap,
                    width > 0 ? width : (element.ImageWidth < 0 ? 40 : element.ImageWidth),
                    height > 0 ? height : (element.ImageHeight < 0 ? 40 : element.ImageHeight), true));
        }
    }
}