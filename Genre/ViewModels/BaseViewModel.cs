﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Runtime.CompilerServices;
using System.Threading.Tasks;
using Genre.Business.Settings;
using Genre.Business.Utils;
using Genre.Models.Abstractions;
using Genre.Models.Models;
using Genre.Pages;
using Xamarin.Forms;

namespace Genre.ViewModels
{
    public class BaseViewModel : INotifyPropertyChanged
    {
        #region Private Properties
        
        private bool _showBackButton;
        private AppDataModel _appData;
        private Lazy<IDataStore> _dataStore;

        #endregion

        #region Commands

        private Command _navigateBackCommand;

        public Command NavigateBackCommand
        {
            get => _navigateBackCommand ??
                   (_navigateBackCommand = new Command(async () => await ExecuteNavigateBackCommand()));
            set => _navigateBackCommand = value;
        }

        #endregion

        #region Properties

        public bool ShowBackButton
        {
            get => _showBackButton;
            set => SetProperty(ref _showBackButton, value);
        }

        public AppDataModel AppData
        {
            get => _appData;
            set => SetProperty(ref _appData, value);
        }

        public IDataStore DataStore => _dataStore.Value;

        #endregion

        #region Constructor

        public BaseViewModel()
        {
            _dataStore = new Lazy<IDataStore>(() => DependencyService.Get<IDataStore>());

            AppData = new AppDataModel();
            AppData.Code = Settings.ApplicantCode;
            if (AppData.Code == 0)
            {
                var generator = new Random();
                AppData.Code = generator.Next(100000, 999999);
                Settings.ApplicantCode = AppData.Code;
            }

            AppData.Applications = InitializeApplications();
            ShowBackButton = true;
        }

        #endregion

        #region Public Methods
        
        public Page GetCurrentPage()
        {
            Page contentPage;
            if (Application.Current.MainPage is MainPage mainPage)
            {
                if (mainPage.Navigation != null && mainPage.Navigation.NavigationStack.Count > 0)
                {
                    int index = mainPage.Navigation.NavigationStack.Count - 1;
                    contentPage = mainPage.Navigation.NavigationStack[index];
                }
                else
                {
                    contentPage = mainPage;
                }
            }
            else
            {
                contentPage = Application.Current.MainPage;
            }

            return contentPage;
        }

        #endregion

        #region Command Executions   

        private async Task ExecuteNavigateBackCommand()
        {
            NavigateBackCommand.CanExecute(false);

            var mainPage = Application.Current.MainPage as NavigationPage;
            if (mainPage != null)
            {
                await mainPage.Navigation.PopPage();
            }

            NavigateBackCommand.CanExecute(true);
        }

        private IEnumerable<ApplicationInfo> InitializeApplications()
        {
            return new List<ApplicationInfo>
            {
                new ApplicationInfo
                {
                    Code = "Pai",
                    Title = "PAI",
                    PieceImageOff = "piece_pai_off.png",
                    PieceImageOn = "piece_pai_on.png",
                    Rated = false,
                    Rectangle = new Rectangle(.5, .5, .614, .542)
                },
                new ApplicationInfo
                {
                    Code = "Thrive",
                    Title = "Thrive",
                    PieceImageOff = "piece_thrive_off.png",
                    PieceImageOn = "piece_thrive_on.png",
                    Rated = false,
                    Rectangle = new Rectangle(.872, .011, .417, .385)
                },
                new ApplicationInfo
                {
                    Code = "Aimo",
                    Title = "AIMO",
                    PieceImageOff = "piece_aimo_off.png",
                    PieceImageOn = "piece_aimo_on.png",
                    Rated = false,
                    Rectangle = new Rectangle(.989, .517, .288, .494)
                },
                new ApplicationInfo
                {
                    Code = "Torafugu",
                    Title = "Torafugu",
                    PieceImageOff = "piece_torafugu_off.png",
                    PieceImageOn = "piece_torafugu_on.png",
                    Rated = false,
                    Rectangle = new Rectangle(.84, .987, .5, .365)
                },
                new ApplicationInfo
                {
                    Code = "Spike",
                    Title = "Spike",
                    PieceImageOff = "piece_spike_off.png",
                    PieceImageOn = "piece_spike_on.png",
                    Rated = false,
                    Rectangle = new Rectangle(.124, .988, .417, .382)
                },
                new ApplicationInfo
                {
                    Code = "Ddm",
                    Title = "DDM",
                    PieceImageOff = "piece_ddm_off.png",
                    PieceImageOn = "piece_ddm_on.png",
                    Rated = false,
                    Rectangle = new Rectangle(.01, .48, .288, .494)
                },
                new ApplicationInfo
                {
                    Code = "TrackActive",
                    Title = "Track Active",
                    PieceImageOff = "piece_trackactive_off.png",
                    PieceImageOn = "piece_trackactive_on.png",
                    Rated = false,
                    Rectangle = new Rectangle(.158, .012, .502, .365)
                }
            };
        }        

        #endregion

        protected bool SetProperty<T>(ref T backingStore, T value,
            [CallerMemberName]string propertyName = "",
            Action onChanged = null)
        {
            if (EqualityComparer<T>.Default.Equals(backingStore, value))
                return false;

            backingStore = value;
            onChanged?.Invoke();
            OnPropertyChanged(propertyName);
            return true;
        }

        #region INotifyPropertyChanged

        public event PropertyChangedEventHandler PropertyChanged;
        protected void OnPropertyChanged([CallerMemberName] string propertyName = "")
        {
            var changed = PropertyChanged;
            changed?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }

        #endregion
    }
}
