﻿using System;
using Genre.Models.Abstractions;
using Xamarin.Forms;

namespace Genre.Elements
{
    public class FormEditor : Editor, IFormInput
    {
        #region Bindable Properties

        public static readonly BindableProperty RequiredProperty =
            BindableProperty.Create(nameof(Required), typeof(bool), typeof(FormEntry),
                false);

        public static readonly BindableProperty ValidationMessageProperty =
            BindableProperty.Create(nameof(ValidationMessage), typeof(string), typeof(FormEntry),
                null);

        public static readonly BindableProperty CustomValidatorProperty =
            BindableProperty.Create(nameof(CustomValidator), typeof(Func<IFormInput, object, bool>), typeof(FormEntry),
                null);

        public bool Required
        {
            get => (bool)GetValue(RequiredProperty);
            set => SetValue(RequiredProperty, value);
        }


        public string ValidationMessage
        {
            get => (string)GetValue(ValidationMessageProperty);
            set => SetValue(ValidationMessageProperty, value);
        }

        public Func<IFormInput, object, bool> CustomValidator
        {
            get => (Func<IFormInput, object, bool>)GetValue(CustomValidatorProperty);
            set => SetValue(CustomValidatorProperty, value);
        }

        #endregion

        #region Public Properties

        public Func<Color, bool> SetLineColor { get; set; }
        public bool Invalid { get; set; }
        public Thickness OldThickness { get; set; }

        #endregion

        #region Public Methods

        public bool IsValid()
        {
            if (!Required) return true;
            if (Required && string.IsNullOrEmpty(Text))
            {
                SetLineColor(Color.Red);
                ShowError("Required");
                Invalid = true;
                if (Invalid)
                    return false;
            }

            if (CustomValidator != null && !CustomValidator(this, Text))
            {
                SetLineColor(Color.Red);
                ShowError(ValidationMessage);
                Invalid = true;
            }
            else
            {
                SetLineColor(default(Color));
                RemoveError();
                Invalid = false;
            }

            return !Invalid;
        }

        public void ShowError(string message)
        {
            if (OldThickness == default(Thickness))
            {
                OldThickness = new Thickness(Margin.Left, Margin.Top, Margin.Right, Margin.Bottom);
            }
            ErrorMessageHandler.ShowError(this, message);
        }

        public void RemoveError()
        {
            ErrorMessageHandler.RemoveError(this, OldThickness);
        }

        #endregion
    }
}
