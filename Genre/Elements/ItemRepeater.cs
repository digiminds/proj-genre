﻿using System;
using System.Collections.Generic;
using Xamarin.Forms;

namespace Genre.Elements
{
    public class ItemRepeater<T> : StackLayout
        where T : class
    {
        
        private IDisposable _collectionChangedHandle;

        #region BindableProperties

        public static BindableProperty ItemsSourceProperty =
            BindableProperty.Create(nameof(ItemsSource),
                typeof(IEnumerable<T>),
                typeof(ItemRepeater<T>),
                propertyChanged: (bindable, oldVal, newVal) =>
                {
                    ((ItemRepeater<T>)bindable).UpdateItemsSource(bindable, oldVal, newVal);
                }, defaultBindingMode: BindingMode.TwoWay
            );

        public IEnumerable<T> ItemsSource
        {
            get => (IEnumerable<T>)GetValue(ItemsSourceProperty);
            set => SetValue(ItemsSourceProperty, value);
        }

        public static BindableProperty OnItemClickedCommandProperty =
            BindableProperty.Create(nameof(OnItemClickedCommand),
                typeof(Command),
                typeof(ItemRepeater<T>));

        public Command OnItemClickedCommand
        {
            get => (Command)GetValue(OnItemClickedCommandProperty);
            set => SetValue(OnItemClickedCommandProperty, value);
        }

        public static BindableProperty ItemTemplateProperty =
            BindableProperty.Create(nameof(ItemTemplate),
                typeof(DataTemplate),
                typeof(ItemRepeater<T>));

        public DataTemplate ItemTemplate
        {
            get => (DataTemplate)GetValue(ItemTemplateProperty);
            set => SetValue(ItemTemplateProperty, value);
        }

        #endregion

        #region Protected Method

        protected virtual View ViewFor(T item)
        {
            var itemTapped = new TapGestureRecognizer()
            {
                Command = new Command(() =>
                {
                    OnItemClickedCommand?.Execute(item);
                }),
                NumberOfTapsRequired = 1
            };

            var itemView = ItemTemplate.CreateContent() as View;
            itemView.BindingContext = new
            {
                Base = BindingContext,
                Model = item
            };
            itemView.GestureRecognizers.Add(itemTapped);

            return itemView;
        }

        #endregion

        #region Private Methods

        private void UpdateItemsSource(BindableObject bindable, object oldValue, object newValue)
        {
            var control = bindable as ItemRepeater<T>;
            if (control == null)
                throw new Exception(
                    "Invalid bindable object passed to ItemRepeater::ItemsChanged expected a ReapterView<T> received a "
                    + bindable.GetType().Name);

            if (control._collectionChangedHandle != null)
            {
                control._collectionChangedHandle.Dispose();
            }

            control._collectionChangedHandle = new CollectionChangedHandle<View, T>(
                control.Children,
                (IEnumerable<T>) newValue,
                control.ViewFor,
                (v, m, i) => control.NotifyItemAdded(v, m));
        }

        private void NotifyItemAdded(View view, T arg2)
        {
            
        }

        #endregion
    }
}
