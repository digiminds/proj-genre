﻿using System;
using Genre.Models.Abstractions;
using Xamarin.Forms;

namespace Genre.Elements
{
    public class FormPicker : Picker, IFormInput
    {
        #region Constructor

        public FormPicker()
        {
            HeightRequest = 50;
        }

        #endregion

        #region Bindable Properties

        public static readonly BindableProperty BorderColorProperty =
            BindableProperty.Create(
                nameof(BorderColor),
                typeof(Color),
                typeof(FormPicker),
                Color.Gray);

        public static readonly BindableProperty BorderWidthProperty =
            BindableProperty.Create(
                nameof(BorderWidth),
                typeof(int),
                typeof(FormPicker));


        public static readonly BindableProperty CornerRadiusProperty =
            BindableProperty.Create(
                nameof(CornerRadius),
                typeof(double),
                typeof(FormPicker));

        public static readonly BindableProperty ImagePathProperty =
            BindableProperty.Create(
                nameof(ImagePath),
                typeof(string),
                typeof(FormPicker));

        public static readonly BindableProperty PlaceholderProperty =
            BindableProperty.Create(
                nameof(Placeholder),
                typeof(string),
                typeof(FormPicker),
                "");

        public static readonly BindableProperty RequiredProperty =
            BindableProperty.Create(nameof(Required), typeof(bool), typeof(FormEntry),
                false);

        public static readonly BindableProperty ValidationMessageProperty =
            BindableProperty.Create(nameof(ValidationMessage), typeof(string), typeof(FormEntry),
                null);

        public static readonly BindableProperty CustomValidatorProperty =
            BindableProperty.Create(nameof(CustomValidator), typeof(Func<IFormInput, object, bool>), typeof(FormEntry),
                null);

        public static readonly BindableProperty IsCurvedCornersEnabledProperty =
            BindableProperty.Create(
                nameof(IsCurvedCornersEnabled),
                typeof(bool),
                typeof(FormPicker),
                true);

        public Color BorderColor
        {
            get => (Color)GetValue(BorderColorProperty);
            set => SetValue(BorderColorProperty, value);
        }

        public int BorderWidth
        {
            get => (int)GetValue(BorderWidthProperty);
            set => SetValue(BorderWidthProperty, value);
        }

        public double CornerRadius
        {
            get => (double)GetValue(CornerRadiusProperty);
            set => SetValue(CornerRadiusProperty, value);
        }

        public string Placeholder
        {
            get => (string)GetValue(PlaceholderProperty);
            set => SetValue(PlaceholderProperty, value);
        }

        public string ImagePath
        {
            get => (string)GetValue(ImagePathProperty);
            set => SetValue(ImagePathProperty, value);
        }

        public bool IsCurvedCornersEnabled
        {
            get => (bool)GetValue(IsCurvedCornersEnabledProperty);
            set => SetValue(IsCurvedCornersEnabledProperty, value);
        }

        public bool Required
        {
            get => (bool)GetValue(RequiredProperty);
            set => SetValue(RequiredProperty, value);
        }

        public string ValidationMessage
        {
            get => (string)GetValue(ValidationMessageProperty);
            set => SetValue(ValidationMessageProperty, value);
        }
        
        public Func<IFormInput, object, bool> CustomValidator
        {
            get => (Func<IFormInput, object, bool>)GetValue(CustomValidatorProperty);
            set => SetValue(CustomValidatorProperty, value);
        }

        #endregion

        #region Public Properties

        public Func<Color, bool> SetLineColor { get; set; }
        public bool Invalid { get; set; }
        public Thickness OldThickness { get; set; }

        #endregion

        #region Public Methods

        public bool IsValid()
        {
            if (!Required) return true;
            if (Required && SelectedItem == null)
            {
                SetLineColor(Color.Red);
                ShowError("Required");
                Invalid = true;
                if (Invalid)
                    return false;
            }

            if (CustomValidator != null && !CustomValidator(this, SelectedItem))
            {
                SetLineColor(Color.Red);
                ShowError(ValidationMessage);
                Invalid = true;
            }
            else
            {
                SetLineColor(default(Color));
                RemoveError();
                Invalid = false;
            }

            return !Invalid;
        }

        public void ShowError(string message)
        {
            if (OldThickness == default(Thickness))
            {
                OldThickness = new Thickness(Margin.Left, Margin.Top, Margin.Right, Margin.Bottom);
            }
            ErrorMessageHandler.ShowError(this, message);
        }

        public void RemoveError()
        {
            ErrorMessageHandler.RemoveError(this, OldThickness);
        }

        #endregion
    }
}
