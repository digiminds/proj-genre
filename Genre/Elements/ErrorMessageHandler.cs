﻿using Xamarin.Forms;

namespace Genre.Elements
{
    public static class ErrorMessageHandler
    {
        public static void ShowError(View view, string message)
        {
            view.Margin = new Thickness(0);

            var layout = view.Parent as StackLayout;
            int viewIndex = layout.Children.IndexOf(view);

            if (viewIndex + 1 < layout.Children.Count)
            {
                View sibling = layout.Children[viewIndex + 1];
                string siblingStyleId = view.Id.ToString();

                if (sibling.StyleId == siblingStyleId)
                {
                    Label errorLabel = sibling as Label;
                    errorLabel.Text = message;
                    errorLabel.IsVisible = true;
                    return;
                }
            }

            layout.Children.Insert(viewIndex + 1, new Label
            {
                Text = message,
                FontSize = 10,
                StyleId = view.Id.ToString(),
                TextColor = Color.Red
            });
        }

        public static void RemoveError(View view, Thickness oldThickness)
        {
            var layout = view.Parent as StackLayout;
            int viewIndex = layout.Children.IndexOf(view);

            if (viewIndex + 1 < layout.Children.Count)
            {
                View sibling = layout.Children[viewIndex + 1];
                string siblingStyleId = view.Id.ToString();

                if (sibling.StyleId == siblingStyleId)
                {
                    sibling.IsVisible = false;
                }
            }

            if (oldThickness != default(Thickness))
            {
                view.Margin = new Thickness(oldThickness.Left, oldThickness.Top, oldThickness.Right, oldThickness.Bottom);
            }
        }
    }
}
