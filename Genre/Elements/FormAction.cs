﻿using System;
using Xamarin.Forms;

namespace Genre.Elements
{
    public class FormAction : StackLayout
    {
        public static BindableProperty ActionTemplateProperty =
            BindableProperty.Create(nameof(ActionTemplate),
                typeof(DataTemplate),
                typeof(FormAction),
                propertyChanged: (bindable, oldVal, newVal) =>
                {
                    ((FormAction)bindable).UpdateAction();
                });

        public static BindableProperty CommandProperty =
            BindableProperty.Create(nameof(Command),
                typeof(Command),
                typeof(FormAction),
                propertyChanged: (bindable, oldVal, newVal) =>
                {
                    ((FormAction)bindable).UpdateCommand(newVal);
                });

        public static BindableProperty ValidationProperty =
            BindableProperty.Create(nameof(Validation),
                typeof(FormActionValidation),
                typeof(FormAction));

        public DataTemplate ActionTemplate
        {
            get => (DataTemplate)GetValue(ActionTemplateProperty);
            set => SetValue(ActionTemplateProperty, value);
        }

        public Command Command
        {
            get => (Command)GetValue(CommandProperty);
            set => SetValue(CommandProperty, value);
        }

        public FormActionValidation Validation
        {
            get => (FormActionValidation)GetValue(ValidationProperty);
            set => SetValue(ValidationProperty, value);
        }

        public Func<bool> IsValidFormFunc { get; set; }

        private void UpdateAction()
        {
            if (ActionTemplate != null)
            {
                Children.Clear();

                var content = ActionTemplate.CreateContent() as View;
                content.GestureRecognizers.Add(new TapGestureRecognizer
                {
                    Command = new Command(() => ExecuteAction()),
                    NumberOfTapsRequired = 1
                });

                Children.Insert(0, content);
            }
        }

        private void UpdateCommand(object newVal)
        {
            var command = (Command)newVal;
        }

        private void ExecuteAction()
        {
            if (Validation == FormActionValidation.OnFormValid)
            {
                if (IsValidFormFunc == null || IsValidFormFunc())
                {
                    Command?.Execute(null);
                }
            }
            else
            {
                Command?.Execute(null);
            }
        }
    }

    public enum FormActionValidation
    {
        None,
        OnFormValid
    }
}
