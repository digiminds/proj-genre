﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Linq;

namespace Genre.Elements
{
    public class CollectionChangedHandle<TSyncType, T> : IDisposable where T : class where TSyncType : class
    {
        private readonly Func<T, TSyncType> _projector;
        private readonly Action<TSyncType, T, int> _postadd;
        private readonly Action<TSyncType> _cleanup;
        private readonly INotifyCollectionChanged _itemsSourceCollectionChangedImplementation;
        private readonly IEnumerable<T> _sourceCollection;
        private readonly IList<TSyncType> _target;

        public CollectionChangedHandle(IList<TSyncType> target, IEnumerable<T> source, Func<T, TSyncType> projector,
            Action<TSyncType, T, int> postadd = null, Action<TSyncType> cleanup = null)
        {
            if (source == null) return;
            _itemsSourceCollectionChangedImplementation = source as INotifyCollectionChanged;
            _sourceCollection = source;
            _target = target;
            _projector = projector;
            _postadd = postadd;
            _cleanup = cleanup;
            InitialPopulation();
            if (_itemsSourceCollectionChangedImplementation == null) return;
            _itemsSourceCollectionChangedImplementation.CollectionChanged += CollectionChanged;
        }

        public void Dispose()
        {
            if (_itemsSourceCollectionChangedImplementation == null) return;
            _itemsSourceCollectionChangedImplementation.CollectionChanged -= CollectionChanged;
        }

        private void CollectionChanged(object sender, NotifyCollectionChangedEventArgs args)
        {
            if (args.Action == NotifyCollectionChangedAction.Reset)
            {
                SafeClearTarget();
            }
            else
            {
                var tlist = new List<T>(_sourceCollection);

                if (args.OldItems != null)
                {
                    var syncitem = _target[args.OldStartingIndex];
                    if (syncitem != null && _cleanup != null) _cleanup(syncitem);
                    _target.RemoveAt(args.OldStartingIndex);
                }

                if (args.NewItems == null) return;
                foreach (var obj in args.NewItems)
                {
                    var item = obj as T;
                    if (item == null) continue;
                    var index = tlist.IndexOf(item);
                    var newsyncitem = _projector(item);
                    _target.Insert(index, newsyncitem);
                    if (_postadd != null) _postadd(newsyncitem, item, index);
                }
            }

        }

        private void InitialPopulation()
        {
            SafeClearTarget();
            foreach (var t in _sourceCollection.Where(x => x != null))
            {
                _target.Add(_projector(t));
            }
        }

        private void SafeClearTarget()
        {
            while (_target.Count > 0)
            {
                var syncitem = _target[0];
                _target.RemoveAt(0);
                _cleanup?.Invoke(syncitem);
            }
        }
    }
}
