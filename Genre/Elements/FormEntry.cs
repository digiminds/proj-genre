﻿using System;
using System.Text.RegularExpressions;
using Genre.Models.Abstractions;
using Xamarin.Forms;

namespace Genre.Elements
{
    public class FormEntry : Entry, IFormInput
    {
        #region Constructor
        
        public FormEntry()
        {
            HeightRequest = 50;
        } 

        #endregion

        #region Bindable Properties

        public static readonly BindableProperty ImageProperty =
            BindableProperty.Create(nameof(Image), typeof(string), typeof(FormEntry), string.Empty);

        public static readonly BindableProperty LineColorProperty =
            BindableProperty.Create(nameof(LineColor), typeof(Color), typeof(FormEntry), Color.White);

        public static readonly BindableProperty ImageHeightProperty =
            BindableProperty.Create(nameof(ImageHeight), typeof(int), typeof(FormEntry), -1);

        public static readonly BindableProperty ImageWidthProperty =
            BindableProperty.Create(nameof(ImageWidth), typeof(int), typeof(FormEntry), -1);

        public static readonly BindableProperty ImageAlignmentProperty =
            BindableProperty.Create(nameof(ImageAlignment), typeof(ImageAlignment), typeof(FormEntry),
                ImageAlignment.Default);

        public static readonly BindableProperty RequiredProperty =
            BindableProperty.Create(nameof(Required), typeof(bool), typeof(FormEntry),
                false);

        public static readonly BindableProperty ValidationProperty =
            BindableProperty.Create(nameof(Validation), typeof(ValidationType), typeof(FormEntry),
                ValidationType.None);

        public static readonly BindableProperty ValidationMessageProperty =
            BindableProperty.Create(nameof(ValidationMessage), typeof(string), typeof(FormEntry),
                null);

        public static readonly BindableProperty CustomValidatorProperty =
            BindableProperty.Create(nameof(CustomValidator), typeof(Func<IFormInput, object, bool>), typeof(FormEntry),
                null);

        public Color LineColor
        {
            get => (Color)GetValue(LineColorProperty);
            set => SetValue(LineColorProperty, value);
        }

        public int ImageWidth
        {
            get => (int)GetValue(ImageWidthProperty);
            set => SetValue(ImageWidthProperty, value);
        }

        public int ImageHeight
        {
            get => (int)GetValue(ImageHeightProperty);
            set => SetValue(ImageHeightProperty, value);
        }

        public string Image
        {
            get => (string)GetValue(ImageProperty);
            set => SetValue(ImageProperty, value);
        }

        public ImageAlignment ImageAlignment
        {
            get => (ImageAlignment)GetValue(ImageAlignmentProperty);
            set => SetValue(ImageAlignmentProperty, value);
        }

        public bool Required
        {
            get => (bool)GetValue(RequiredProperty);
            set => SetValue(RequiredProperty, value);
        }

        public ValidationType Validation
        {
            get => (ValidationType)GetValue(ValidationProperty);
            set => SetValue(ValidationProperty, value);
        }

        public string ValidationMessage
        {
            get => (string)GetValue(ValidationMessageProperty);
            set => SetValue(ValidationMessageProperty, value);
        }

        public Func<IFormInput, object, bool> CustomValidator
        {
            get => (Func<IFormInput, object, bool>)GetValue(CustomValidatorProperty);
            set => SetValue(CustomValidatorProperty, value);
        }

        #endregion

        #region Public Properties

        public Func<Color, bool> SetLineColor { get; set; }
        public bool Invalid { get; set; }
        public Thickness OldThickness { get; set; }

        #endregion

        #region Public Methods

        public bool IsValid()
        {
            if (!Required) return true;
            if (Required && string.IsNullOrEmpty(Text))
            {
                SetLineColor(Color.Red);
                ShowError("Required");
                Invalid = true;
                if (Invalid)
                    return false;
            }

            switch (Validation)
            {
                case ValidationType.Email:
                {
                    var email = Text;
                    if (!string.IsNullOrEmpty(email))
                    {
                        var reg = @"^[\w!#$%&'*+\-/=?\^_`{|}~]+(\.[\w!#$%&'*+\-/=?\^_`{|}~]+)*" + "@" +
                                  @"((([\-\w]+\.)+[a-zA-Z]{2,4})|(([0-9]{1,3}\.){3}[0-9]{1,3}))$";

                        if (!Regex.IsMatch(email, reg))
                        {
                            SetLineColor(Color.Red);
                            ShowError(!string.IsNullOrEmpty(ValidationMessage)
                                ? ValidationMessage
                                : "Invalid Email Form");
                            Invalid = true;
                        }
                        else
                        {
                            SetLineColor(default(Color));
                            RemoveError();
                            Invalid = false;
                        }
                    }
                }
                    break;
                //case ValidationType.Mobile:
                //{
                //    var mobile = Text;
                //    if (!string.IsNullOrEmpty(mobile))
                //    {
                //        var reg = "^(9[0-9][0-9])([0-9]){1,}$"; //"^(009665|9665|\\+9665)(5|0|3|6|4|9|1|8|7)([0-9]{7})$"; //"^(009665|9665|\\+9665|05|5)(5|0|3|6|4|9|1|8|7)([0-9]{7})$";

                //        if (!Regex.IsMatch(mobile, reg))
                //        {
                //            SetLineColor(Color.Red);
                //            ShowError(!string.IsNullOrEmpty(ValidationMessage)
                //                ? ValidationMessage
                //                : AppResources.InvalidMobileFormat);
                //            Invalid = true;
                //        }
                //        else
                //        {
                //            SetLineColor(default(Color));
                //            RemoveError();
                //            Invalid = false;
                //        }
                //    }
                //}
                //    break;
                case ValidationType.Number:
                {
                    var number = Text;
                    if (!string.IsNullOrEmpty(number))
                    {
                        if (!decimal.TryParse(number, out var dec))
                        {
                            SetLineColor(Color.Red);
                            ShowError(!string.IsNullOrEmpty(ValidationMessage)
                                ? ValidationMessage
                                : "Value should be numeric");
                            Invalid = true;
                        }
                        else
                        {
                            SetLineColor(default(Color));
                            RemoveError();
                            Invalid = false;
                        }
                    }
                }
                    break;
                case ValidationType.Custom:
                {
                    if (CustomValidator != null && !CustomValidator(this, Text))
                    {
                        SetLineColor(Color.Red);
                        ShowError(ValidationMessage);
                        Invalid = true;
                    }
                    else
                    {
                        SetLineColor(default(Color));
                        RemoveError();
                        Invalid = false;
                    }
                }
                    break;
                default:
                {
                    SetLineColor(default(Color));
                    RemoveError();
                    Invalid = false;
                }
                    break;
            }

            return !Invalid;
        }

        public void ShowError(string message)
        {
            if (OldThickness == default(Thickness))
            {
                OldThickness = new Thickness(Margin.Left, Margin.Top, Margin.Right, Margin.Bottom);
            }
            ErrorMessageHandler.ShowError(this, message);
        }

        public void RemoveError()
        {
            ErrorMessageHandler.RemoveError(this, OldThickness);
        }

        #endregion
    }

    public enum ImageAlignment
    {
        Default,
        Left,
        Right
    }

    public enum ValidationType
    {
        None,
        Email,
        Mobile,
        Number,
        Custom
    }
}
