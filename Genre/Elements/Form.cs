﻿using System.Collections.Generic;
using System.Linq;
using Genre.Models.Abstractions;
using Xamarin.Forms;

namespace Genre.Elements
{
    public class Form : StackLayout
    {
        private List<FormAction> _actions;
        private List<IFormInput> _entries;

        public static BindableProperty FormTemplateProperty =
            BindableProperty.Create(nameof(FormTemplate),
                typeof(DataTemplate),
                typeof(Form),
                propertyChanged: (bindable, oldVal, newVal) =>
                {
                    ((Form)bindable).UpdateForm();
                });

        public DataTemplate FormTemplate
        {
            get => (DataTemplate)GetValue(FormTemplateProperty);
            set => SetValue(FormTemplateProperty, value);
        }

        public Form()
        {
            _entries = new List<IFormInput>();
            _actions = new List<FormAction>();
        }

        private void UpdateForm()
        {
            if (FormTemplate != null)
            {
                Children.Clear();

                var content = FormTemplate.CreateContent() as View;
                IterateChildrenRec(content.LogicalChildren);

                Children.Insert(0, content);
            }
        }

        private void IterateChildrenRec(IReadOnlyCollection<Element> elements)
        {
            foreach (var element in elements)
            {
                if (element is FormAction action)
                {
                    action.IsValidFormFunc = IsValidFormFunc;
                    _actions.Add(action);
                }
                else if (element is IFormInput entry)
                {
                    _entries.Add(entry);
                }
                else if (element is Layout layout)
                {
                    IterateChildrenRec(element.LogicalChildren);
                }
            }
        }

        private bool IsValidFormFunc()
        {
            if (_entries.Any())
            {
                foreach (var entry in _entries)
                {
                    if (!entry.IsValid())
                    {
                        return false;
                    }
                }
            }

            return true;
        }
    }
}
