﻿using System;
using Genre.Models.Abstractions;
using Xamarin.Forms;

namespace Genre.Elements
{
    public class FormDatePicker : DatePicker, IFormInput
    {
        #region Constructor

        public FormDatePicker()
        {
            HeightRequest = 50;
        }

        #endregion

        #region Bindable Properties

        public static readonly BindableProperty LineColorProperty =
            BindableProperty.Create(nameof(LineColor), typeof(Color), typeof(FormDatePicker), Color.White);

        public static readonly BindableProperty PlaceholderProperty =
            BindableProperty.Create(nameof(Placeholder), typeof(string), typeof(FormDatePicker));

        public static readonly BindableProperty PlaceholderColorProperty =
            BindableProperty.Create(nameof(PlaceholderColor), typeof(Color), typeof(FormDatePicker), Color.White);

        public static readonly BindableProperty RequiredProperty =
            BindableProperty.Create(nameof(Required), typeof(bool), typeof(FormDatePicker),
                false);

        public static readonly BindableProperty ValidationMessageProperty =
            BindableProperty.Create(nameof(ValidationMessage), typeof(string), typeof(FormDatePicker),
                null);

        public static readonly BindableProperty CustomValidatorProperty =
            BindableProperty.Create(nameof(CustomValidator), typeof(Func<IFormInput, object, bool>), typeof(FormDatePicker),
                null);

        public Color LineColor
        {
            get => (Color)GetValue(LineColorProperty);
            set => SetValue(LineColorProperty, value);
        }

        public string Placeholder
        {
            get => (string)GetValue(PlaceholderProperty);
            set => SetValue(PlaceholderProperty, value);
        }

        public Color PlaceholderColor
        {
            get => (Color)GetValue(PlaceholderColorProperty);
            set => SetValue(PlaceholderColorProperty, value);
        }

        public bool Required
        {
            get => (bool)GetValue(RequiredProperty);
            set => SetValue(RequiredProperty, value);
        }

        public string ValidationMessage
        {
            get => (string)GetValue(ValidationMessageProperty);
            set => SetValue(ValidationMessageProperty, value);
        }

        public Func<IFormInput, object, bool> CustomValidator
        {
            get => (Func<IFormInput, object, bool>)GetValue(CustomValidatorProperty);
            set => SetValue(CustomValidatorProperty, value);
        }

        #endregion

        #region Public Properties
        
        public bool Invalid { get; set; }
        public Func<Color, bool> SetLineColor { get; set; }
        public Thickness OldThickness { get; set; }

        #endregion

        #region Public Methods

        public bool IsValid()
        {
            if (!Required) return true;
            if (Required && Date == default(DateTime))
            {
                SetLineColor(Color.Red);
                ShowError("Required");
                Invalid = true;
                if (Invalid)
                    return false;
            }

            if (CustomValidator != null && !CustomValidator(this, Date))
            {
                SetLineColor(Color.Red);
                ShowError(ValidationMessage);
                Invalid = true;
            }
            else
            {
                SetLineColor(default(Color));
                RemoveError();
                Invalid = false;
            }

            return !Invalid; ;
        }

        public void ShowError(string message)
        {
            if (OldThickness == default(Thickness))
            {
                OldThickness = new Thickness(Margin.Left, Margin.Top, Margin.Right, Margin.Bottom);
            }
            ErrorMessageHandler.ShowError(this, message);
        }

        public void RemoveError()
        {
            ErrorMessageHandler.RemoveError(this, OldThickness);
        }

        #endregion
    }
}
