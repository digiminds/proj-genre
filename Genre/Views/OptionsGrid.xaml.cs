﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using Genre.Models.Models;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace Genre.Views
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class OptionsGrid : ContentView
    {
        public static BindableProperty OptionsProperty =
            BindableProperty.Create(nameof(Options),
                typeof(IEnumerable<Tupple>),
                typeof(OptionsGrid),
                propertyChanged: (bindable, oldVal, newVal) =>
                {
                    ((OptionsGrid)bindable).UpdateOptions();
                });

        public static BindableProperty ColumnsNumberProperty =
            BindableProperty.Create(nameof(ColumnsNumber),
                typeof(int),
                typeof(OptionsGrid),
                propertyChanged: (bindable, oldVal, newVal) =>
                {
                    ((OptionsGrid)bindable).UpdateOptions();
                });

        public static BindableProperty OptionTemplateProperty =
            BindableProperty.Create(nameof(OptionTemplate),
                typeof(DataTemplate),
                typeof(OptionsGrid),
                propertyChanged: (bindable, oldVal, newVal) =>
                {
                    ((OptionsGrid)bindable).UpdateOptions();
                });

        public static BindableProperty MultipleProperty =
            BindableProperty.Create(nameof(Multiple),
                typeof(bool),
                typeof(OptionsGrid),
                propertyChanged: (bindable, oldVal, newVal) =>
                {
                    ((OptionsGrid)bindable).UpdateOptions();
                });

        public static BindableProperty NotClickableProperty =
            BindableProperty.Create(nameof(NotClickable),
                typeof(bool),
                typeof(OptionsGrid),
                propertyChanged: (bindable, oldVal, newVal) =>
                {
                    ((OptionsGrid)bindable).UpdateOptions();
                });

        public static BindableProperty CanDeselectProperty =
            BindableProperty.Create(nameof(CanDeselect),
                typeof(bool),
                typeof(OptionsGrid),
                propertyChanged: (bindable, oldVal, newVal) =>
                {
                    ((OptionsGrid)bindable).UpdateOptions();
                });

        public static BindableProperty OnChangedCommandProperty =
            BindableProperty.Create(nameof(OnChangedCommand),
                typeof(Command),
                typeof(OptionsGrid));

        public static BindableProperty ValueProperty =
            BindableProperty.Create(nameof(Value),
                typeof(object),
                typeof(OptionsGrid),
                defaultBindingMode: BindingMode.TwoWay,
                propertyChanged: (bindable, oldVal, newVal) =>
                {
                    ((OptionsGrid)bindable).ValueChanged(oldVal, newVal);
                });

        public IEnumerable<Tupple> Options
        {
            get => (IEnumerable<Tupple>)GetValue(OptionsProperty);
            set => SetValue(OptionsProperty, value);
        }

        public int? ColumnsNumber
        {
            get => (int)GetValue(ColumnsNumberProperty);
            set => SetValue(ColumnsNumberProperty, value);
        }

        public DataTemplate OptionTemplate
        {
            get => (DataTemplate)GetValue(OptionTemplateProperty);
            set => SetValue(OptionTemplateProperty, value);
        }

        public bool Multiple
        {
            get => (bool)GetValue(MultipleProperty);
            set => SetValue(MultipleProperty, value);
        }

        public bool NotClickable
        {
            get => (bool)GetValue(NotClickableProperty);
            set => SetValue(NotClickableProperty, value);
        }

        public bool CanDeselect
        {
            get => (bool)GetValue(CanDeselectProperty);
            set => SetValue(CanDeselectProperty, value);
        }

        public Command OnChangedCommand
        {
            get => (Command)GetValue(OnChangedCommandProperty);
            set => SetValue(OnChangedCommandProperty, value);
        }

        public object Value
        {
            get => GetValue(ValueProperty);
            set => SetValue(ValueProperty, value);
        }

        public OptionsGrid()
        {
            InitializeComponent();
        }

        private void UpdateOptions()
        {
            var columnsNumber = ColumnsNumber ?? 3;

            Grid.ColumnDefinitions.Clear();
            Grid.RowDefinitions.Clear();
            Grid.Children.Clear();

            if (Options != null && Options.Any())
            {
                for (var i = 0; i < columnsNumber; i++)
                {
                    Grid.ColumnDefinitions.Add(new ColumnDefinition());
                }
                Grid.RowDefinitions.Add(new RowDefinition { Height = GridLength.Auto });

                int row = 0, col = 0;
                foreach (var option in Options)
                {
                    var optionTapped = new TapGestureRecognizer()
                    {
                        Command = new Command(() => OptionTapped(option.Id)),
                        NumberOfTapsRequired = 1
                    };

                    var itemView = OptionTemplate.CreateContent() as View;
                    itemView.BindingContext = new
                    {
                        Value = option.Id,
                        option.Name,
                        option.Title,
                        option.IsSelected
                    };
                    itemView.GestureRecognizers.Add(optionTapped);

                    Grid.Children.Add(itemView, col, row);
                    Grid.SetColumnSpan(itemView, option.ColSpan);
                    col++;

                    if (col % columnsNumber == 0)
                    {
                        col = 0;
                        row++;
                    }
                }
            }
        }

        private void OptionTapped(object id)
        {
            if (NotClickable) return;

            var oldValue = Value;
            if (Multiple)
            {
                if (Value == null)
                {
                    Value = new List<object> { id };
                }
                else
                {
                    var values = (IList)Value;

                    if (CanDeselect && values.Contains(id))
                    {
                        values.Remove(id);
                    }
                    else
                    {
                        values.Add(id);
                    }
                    Value = values;
                    ValueChanged(oldValue, values);
                }
            }
            else
            {
                if (CanDeselect && oldValue == id)
                    id = null;

                Value = id;
            }
        }

        private void ValueChanged(object oldValue, object newValue)
        {
            if (oldValue != newValue)
                OnChangedCommand?.Execute(newValue);

            var options = new List<Tupple>();
            options.AddRange(Options);

            foreach (var option in options)
            {
                if (Multiple)
                {
                    var values = (IList)Value;
                    option.IsSelected = values != null && values.Contains(option.Id);
                }
                else
                {
                    option.IsSelected = newValue != null && option.Id.Equals(newValue);
                }
            }

            Options = options;
        }
    }
}