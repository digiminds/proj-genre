﻿using Genre.Pages;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

[assembly: XamlCompilation(XamlCompilationOptions.Compile)]
namespace Genre
{
    public partial class App : Application
    {
        public App()
        {
            InitializeComponent();
        }

        public static void InitializeApp()
        {
            Current.MainPage = new NavigationPage(new MainPage());
        }

        protected override void OnStart()
        {
            InitializeApp();
        }

        protected override void OnSleep()
        {
            // Handle when your app sleeps
        }

        protected override void OnResume()
        {
            // Handle when your app resumes
        }

        public bool DoBack
        {
            get
            {
                if (MainPage is NavigationPage mainPage)
                {
                    return mainPage.Navigation.NavigationStack.Count > 1;
                }
                return false;
            }
        }
    }
}
