﻿using System.Linq;
using Genre.Business.Utils;
using Genre.ViewModels;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace Genre.Pages
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class PuzzlePage : ContentPage
    {
        private BaseViewModel _baseViewModel;
        public PuzzlePage()
        {
            InitializeComponent();
            NavigationPage.SetHasNavigationBar(this, false);
        }
        protected override void OnAppearing()
        {
            base.OnAppearing();
            _baseViewModel = BindingContext as BaseViewModel;
            _baseViewModel.ShowBackButton = true;
            LoadApplications();
        }

        private async void PieceTapped(object sender, object e)
        {
            var senderElement = sender as Image;
            if (sender != null)
            {
                var selectedApp =
                    _baseViewModel.AppData.Applications.FirstOrDefault(a => a.Code.Equals(senderElement.ClassId));
                selectedApp.Rated = true;
                await Navigation.PushPage(new AppRatingPage(senderElement.ClassId)
                {
                    BindingContext = _baseViewModel
                });
            }
        }

        private void LoadApplications()
        {
            AppPieces.Children.Clear();
            foreach (var application in _baseViewModel.AppData.Applications)
            {
                var piece = new Image
                {
                    ClassId = application.Code,
                    Source = application.PieceImage,
                };
                AbsoluteLayout.SetLayoutBounds(piece, application.Rectangle);
                AbsoluteLayout.SetLayoutFlags(piece, AbsoluteLayoutFlags.All);
                piece.GestureRecognizers.Add(new TapGestureRecognizer
                { TappedCallback = (sender, e) => PieceTapped(sender, e) });
                AppPieces.Children.Add(piece);
            }
        }
    }
}