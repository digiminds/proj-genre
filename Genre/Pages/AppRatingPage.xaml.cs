﻿using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace Genre.Pages
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class AppRatingPage : ContentPage
    {
        private string appName;
		public AppRatingPage (string app)
		{
			InitializeComponent ();
            NavigationPage.SetHasNavigationBar(this, false);
            appName = app;
        }

        protected override void OnAppearing()
        {
            base.OnAppearing();
            AppName.Text = appName;
        }
    }
}