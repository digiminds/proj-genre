﻿using System;
using Genre.Business.Utils;
using Genre.ViewModels;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace Genre.Pages
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class AgePage : ContentPage
    {
        private BaseViewModel _baseViewModel;
		public AgePage ()
		{
			InitializeComponent ();
            NavigationPage.SetHasNavigationBar(this, false);
        }

        protected override void OnAppearing()
        {
            base.OnAppearing();
            _baseViewModel = BindingContext as BaseViewModel;
            _baseViewModel.ShowBackButton = true;
        }

        private async void AgeSelected(object sender, EventArgs e)
        {
            await Navigation.PushPage(new PuzzlePage()
            {
                BindingContext = _baseViewModel
            });
        }
    }
}