﻿using System;
using Genre.Business.Utils;
using Genre.Models.Models;
using Genre.ViewModels;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace Genre.Pages
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class MainPage : ContentPage
    {
        private readonly BaseViewModel _baseViewModel;
        public MainPage()
        {
            InitializeComponent();
            NavigationPage.SetHasNavigationBar(this, false);
            BindingContext = _baseViewModel = new BaseViewModel();
        }

        protected override void OnAppearing()
        {
            base.OnAppearing();
            _baseViewModel.ShowBackButton = false;
        }

        private async void MaleImageTapped(object sender, EventArgs e)
        {
            _baseViewModel.AppData.ApplicantInfo.Gender = Gender.Male;
            await Navigation.PushPage(new AgePage()
            {
                BindingContext = _baseViewModel
            });
        }

        private async void FemaleImageTapped(object sender, EventArgs e)
        {
            _baseViewModel.AppData.ApplicantInfo.Gender = Gender.Female;
            await Navigation.PushPage(new AgePage()
            {
                BindingContext = _baseViewModel
            });
        }
    }
}