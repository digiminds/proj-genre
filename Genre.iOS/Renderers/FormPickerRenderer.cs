﻿using System;
using System.ComponentModel;
using CoreGraphics;
using Foundation;
using Genre.Elements;
using Genre.iOS.Renderers;
using UIKit;
using Xamarin.Forms;
using Xamarin.Forms.Platform.iOS;

[assembly: ExportRenderer(typeof(FormPicker), typeof(FormPickerRenderer))]
namespace Genre.iOS.Renderers
{
    public class FormPickerRenderer : PickerRenderer
    {
        private readonly FlowDirection _flowDirection = Device.FlowDirection;
        FormPicker element;

        protected override void OnElementChanged(ElementChangedEventArgs<Picker> e)
        {
            base.OnElementChanged(e);

            if (e.NewElement != null)
            {
                element = (FormPicker)Element;

                if (element.IsCurvedCornersEnabled)
                {
                    Control.LeftView = new UIView(new CGRect(0f, 0f, 9f, 20f));
                    Control.LeftViewMode = UITextFieldViewMode.Always;
                    Control.TextAlignment = _flowDirection == FlowDirection.RightToLeft
                        ? UITextAlignment.Right
                        : UITextAlignment.Left;
                    Control.KeyboardAppearance = UIKeyboardAppearance.Dark;
                    Control.ReturnKeyType = UIReturnKeyType.Done;
                    Control.Layer.CornerRadius = Convert.ToSingle(element.CornerRadius);
                    Control.Layer.BorderColor = element.BorderColor.ToCGColor();
                    Control.Layer.BorderWidth = element.BorderWidth;

                    Control.ClipsToBounds = true;
                    var imagePath = element.ImagePath;

                    if (string.IsNullOrEmpty(imagePath))
                        imagePath = "picker_icon";
                    if (imagePath.Contains("."))
                        imagePath = imagePath.Split('.')[0];

                    var downarrow = UIImage.FromBundle(imagePath);
                    Control.RightViewMode = UITextFieldViewMode.Always;
                    Control.RightView = new UIImageView(downarrow);
                }
                else
                {
                    element.SetLineColor = SetLineColor;
                    if (!string.IsNullOrEmpty(element.Placeholder))
                    {
                        Control.Text = element.Required ? $"{element.Placeholder} *" : element.Placeholder;
                    }
                    SetCompound();
                    SetLineColor();
                }
            }
        }

        protected override void OnElementPropertyChanged(object sender, PropertyChangedEventArgs e)
        {
            base.OnElementPropertyChanged(sender, e);

            if (e.PropertyName.Equals("IsFocused"))
            {
                element = (FormPicker)Element;
                if (element.IsFocused == false)
                {
                    element.IsValid();
                }
            }
        }

        private void SetCompound()
        {
            var downarrow = UIImage.FromBundle("picker_icon");
            Control.RightViewMode = UITextFieldViewMode.Always;
            Control.RightView = new UIImageView(downarrow);

            Control.TextAlignment = _flowDirection == FlowDirection.RightToLeft
                ? UITextAlignment.Right
                : UITextAlignment.Left;
        }

        private bool SetLineColor(Color color = default(Color))
        {
            var lineView = new UIView
            {
                BackgroundColor = color == default(Color) ? element.BorderColor.ToUIColor() : color.ToUIColor(),
                TranslatesAutoresizingMaskIntoConstraints = false
            };
            Control.BorderStyle = UITextBorderStyle.None;
            Control.AddSubview(lineView);

            var metrics = new NSDictionary("width", new NSNumber(1));
            var views = new NSDictionary("lineView", lineView);

            Control.AddConstraints(NSLayoutConstraint.FromVisualFormat("H:|[lineView]|", NSLayoutFormatOptions.DirectionLeadingToTrailing, metrics, views));
            Control.AddConstraints(NSLayoutConstraint.FromVisualFormat("V:[lineView(width)]|", NSLayoutFormatOptions.DirectionLeadingToTrailing, metrics, views));

            return true;
        }
    }
}