﻿using System.ComponentModel;
using CoreGraphics;
using Genre.Elements;
using Genre.iOS.Renderers;
using UIKit;
using Xamarin.Forms;
using Xamarin.Forms.Platform.iOS;

[assembly: ExportRenderer(typeof(FormEditor), typeof(FormEditorRenderer))]
namespace Genre.iOS.Renderers
{
    public class FormEditorRenderer : EditorRenderer
    {
        FormEditor element;
        UIView lineView;
        private bool lineAdded;

        public FormEditorRenderer()
        {
            lineView = new UIView();
        }

        protected override void OnElementChanged(ElementChangedEventArgs<Editor> e)
        {
            base.OnElementChanged(e);
            if (e.OldElement != null || e.NewElement == null)
                return;

            element = (FormEditor)Element;
            element.SetLineColor = SetLineColor;

            if (element.Required)
            {
                element.Placeholder = $"{element.Placeholder} *";
            }
            SetLineColor(element.TextColor);
        }

        protected override void OnElementPropertyChanged(object sender, PropertyChangedEventArgs e)
        {
            base.OnElementPropertyChanged(sender, e);

            if (e.PropertyName.Equals("IsFocused"))
            {
                element = (FormEditor)Element;
                if (element.IsFocused == false)
                {
                    element.IsValid();
                }
            }
            else if (e.PropertyName.Equals("Text"))
            {
                lineView.Frame = new CGRect(0, Frame.Height + Control.ContentOffset.Y - 1, Frame.Width, 1);
            }
        }

        private bool SetLineColor(Color color = default(Color))
        {
            lineView.BackgroundColor = color == default(Color) ? element.TextColor.ToUIColor() : color.ToUIColor();
            lineView.Frame = new CGRect(0, Frame.Height + Control.ContentOffset.Y - 1, Frame.Width, 1);

            if (!lineAdded)
            {
                Control.AddSubview(lineView);
                lineAdded = true;
            }

            return true;

            //lineView = new UIView
            //{
            //    BackgroundColor = color == default(Color) ? element.TextColor.ToUIColor() : color.ToUIColor(),
            //    TranslatesAutoresizingMaskIntoConstraints = false,
            //    Frame = new CGRect(0, Frame.Height + Control.ContentOffset.Y - 1, Frame.Width, 1)
            //};
            //Control.AddSubview(lineView);

            //var metrics = new NSDictionary("width", new NSNumber(1));
            //var views = new NSDictionary("lineView", lineView);

            //Control.AddConstraints(NSLayoutConstraint.FromVisualFormat("H:|[lineView]|", NSLayoutFormatOptions.DirectionLeadingToTrailing, metrics, views));
            //Control.AddConstraints(NSLayoutConstraint.FromVisualFormat("V:[lineView(width)]|", NSLayoutFormatOptions.DirectionLeadingToTrailing, metrics, views));

            //return true;
        }
    }
}