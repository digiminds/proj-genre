﻿using System.ComponentModel;
using System.Drawing;
using CoreGraphics;
using Foundation;
using Genre.Elements;
using Genre.iOS.Renderers;
using UIKit;
using Xamarin.Forms;
using Xamarin.Forms.Platform.iOS;
using Color = Xamarin.Forms.Color;

[assembly: ExportRenderer(typeof(FormEntry), typeof(FormEntryRenderer))]
namespace Genre.iOS.Renderers
{
    public class FormEntryRenderer : EntryRenderer
    {
        FormEntry element;
        private bool mobileChanges = false;
        protected override void OnElementChanged(ElementChangedEventArgs<Entry> e)
        {
            base.OnElementChanged(e);

            if (e.OldElement != null || e.NewElement == null)
                return;

            element = (FormEntry)Element;
            element.SetLineColor = SetLineColor;

            if (element.Required)
            {
                element.Placeholder = $"{element.Placeholder} *";
            }

            SetCompound();
            SetLineColor();            
        }

        protected override void OnElementPropertyChanged(object sender, PropertyChangedEventArgs e)
        {
            base.OnElementPropertyChanged(sender, e);

            if (e.PropertyName.Equals("IsFocused"))
            {
                element = (FormEntry)Element;
                if (element.IsFocused == false)
                {
                    element.IsValid();
                }
            }
            else if (e.PropertyName.Equals("Text"))
            {
                //if (element.Validation == ValidationType.Mobile)
                //{
                //    if (!mobileChanges && !string.IsNullOrEmpty(element.Text) && element.Text.Length == 1)
                //    {
                //        element.Text = "966" + element.Text;
                //        mobileChanges = true;
                //    }
                //}
                if (element.Invalid)
                {
                    element.IsValid();
                }
            }
        }

        private void SetCompound()
        {
            if (!string.IsNullOrEmpty(element.Image))
            {
                switch (element.ImageAlignment)
                {
                    case ImageAlignment.Left:
                        Control.LeftViewMode = UITextFieldViewMode.Always;
                        Control.LeftView = GetImageView(element.Image, element.ImageHeight < 0 ? 20 : element.ImageHeight, element.ImageWidth < 0 ? 20 : element.ImageWidth);
                        break;
                    case ImageAlignment.Right:
                        Control.RightViewMode = UITextFieldViewMode.Always;
                        Control.RightView = GetImageView(element.Image, element.ImageHeight < 0 ? 20 : element.ImageHeight, element.ImageWidth < 0 ? 20 : element.ImageWidth);
                        break;
                    case ImageAlignment.Default:
                        goto case ImageAlignment.Left;

                }
            }
            
            var flowDirection = Device.FlowDirection;
            if (flowDirection == FlowDirection.RightToLeft)
                Control.TextAlignment = UITextAlignment.Right;
        }

        private bool SetLineColor(Color color = default(Color))
        {
            var lineView = new UIView
            {
                BackgroundColor = color == default(Color) ? element.LineColor.ToUIColor() : color.ToUIColor(),
                TranslatesAutoresizingMaskIntoConstraints = false
            };
            Control.BorderStyle = UITextBorderStyle.None;
            Control.AddSubview(lineView);

            var metrics = new NSDictionary("width", new NSNumber(1));
            var views = new NSDictionary("lineView", lineView);

            Control.AddConstraints(NSLayoutConstraint.FromVisualFormat("H:|[lineView]|", NSLayoutFormatOptions.DirectionLeadingToTrailing, metrics, views));
            Control.AddConstraints(NSLayoutConstraint.FromVisualFormat("V:[lineView(width)]|", NSLayoutFormatOptions.DirectionLeadingToTrailing, metrics, views));

            return true;
        }

        private UIView GetImageView(string imagePath, int height, int width)
        {
            var uiImageView = new UIImageView(UIImage.FromBundle(imagePath))
            {
                Frame = new RectangleF(0, 0, width, height),
                Center = new CGPoint((width + 10) / 2.0, height / 2.0)
            };
            UIView objLeftView = new UIView(new System.Drawing.Rectangle(0, 0, width + 10, height));            
            objLeftView.AddSubview(uiImageView);            

            return objLeftView;
        }
    }
}