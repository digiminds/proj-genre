﻿using System.ComponentModel;
using Foundation;
using Genre.Elements;
using Genre.iOS.Renderers;
using UIKit;
using Xamarin.Forms;
using Xamarin.Forms.Platform.iOS;

[assembly: ExportRenderer(typeof(FormDatePicker), typeof(FormDatePickerRenderer))]
namespace Genre.iOS.Renderers
{
    public class FormDatePickerRenderer : DatePickerRenderer
    {
        FormDatePicker element;
        protected override void OnElementChanged(ElementChangedEventArgs<DatePicker> e)
        {
            base.OnElementChanged(e);

            if (e.OldElement != null || e.NewElement == null)
                return;

            element = (FormDatePicker)Element;
            element.SetLineColor = SetLineColor;

            if (!string.IsNullOrEmpty(element.Placeholder))
            {
                Control.Text = element.Required ? $"{element.Placeholder} *" : element.Placeholder;
            }

            SetLineColor();
        }

        protected override void OnElementPropertyChanged(object sender, PropertyChangedEventArgs e)
        {
            base.OnElementPropertyChanged(sender, e);

            if (e.PropertyName.Equals("IsFocused"))
            {
                element = (FormDatePicker)Element;
                if (element.IsFocused == false)
                {
                    element.IsValid();
                }
            }
        }

        private bool SetLineColor(Color color = default(Color))
        {
            var lineView = new UIView
            {
                BackgroundColor = color == default(Color) ? element.LineColor.ToUIColor() : color.ToUIColor(),
                TranslatesAutoresizingMaskIntoConstraints = false
            };
            Control.BorderStyle = UITextBorderStyle.None;
            Control.AddSubview(lineView);

            var metrics = new NSDictionary("width", new NSNumber(1));
            var views = new NSDictionary("lineView", lineView);

            Control.AddConstraints(NSLayoutConstraint.FromVisualFormat("H:|[lineView]|", NSLayoutFormatOptions.DirectionLeadingToTrailing, metrics, views));
            Control.AddConstraints(NSLayoutConstraint.FromVisualFormat("V:[lineView(width)]|", NSLayoutFormatOptions.DirectionLeadingToTrailing, metrics, views));

            return true;
        }
    }
}