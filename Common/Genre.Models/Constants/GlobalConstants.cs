﻿namespace Genre.Models.Constants
{
    public static class GlobalConstants
    {
        public class Values
        {
            public const string DefaultDateFormat = "dd, MMM yyyy";
        }

        public class Messages
        {
        }

        public class Icons
        {
        }

        public class Colors
        {
        }
    }
}
