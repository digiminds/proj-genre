﻿namespace Genre.Models.Models
{
    public class LovItem
    {
        public string Id { get; set; }
        public string Code { get; set; }
        public string Title { get; set; }
        public string ParentId { get; set; }

        public LovItem(string code, string title)
        {
            Code = code;
            Title = title;
        }

        public LovItem(string id, string code, string title, string parentId = null)
        {
            Id = id;
            Code = code;
            Title = title;
            ParentId = parentId;
        }
    }
}
