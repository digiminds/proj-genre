﻿namespace Genre.Models.Models
{
    public class Tupple
    {
        public object Id { get; set; }
        public string Name { get; set; }
        public string Title { get; set; }
        public int ColSpan { get; set; }
        public bool IsSelected { get; set; }

        public Tupple(object id, string title, bool isSelected = false, int colSpan = 1)
        {
            Id = id;
            Title = title;
            IsSelected = isSelected;
            ColSpan = colSpan;
        }

        public Tupple()
        {
            ColSpan = 1;
        }
    }
}
