﻿using System;
using System.Linq;
using System.Reflection;
using Genre.Models.Abstractions;

namespace Genre.Models.Models
{
    public class FormModel : IFormModel
    {
        public ValidationResult IsValid()
        {
            var type = GetType();
            var properties = type.GetProperties(BindingFlags.Instance | BindingFlags.Public);
            foreach (var property in properties)
            {
                var attribute = property.GetCustomAttributes<RequiredAttribute>();
                if (!attribute.Any()) continue;

                var value = property.GetValue(this, null);
                var propType = property.PropertyType;

                if (string.IsNullOrEmpty(value?.ToString()))
                {
                    return new ValidationResult
                    {
                        IsValid = false,
                        Type = ValidationType.FormValidation,
                        Message = property.Name
                    };
                }

                if (propType == typeof(DateTime) && (DateTime)value == default(DateTime))
                {
                    return new ValidationResult
                    {
                        IsValid = false,
                        Type = ValidationType.FormValidation,
                        Message = property.Name
                    };
                }
            }

            return new ValidationResult
            {
                IsValid = true
            };
        }
    }

    public class ValidationResult
    {
        public bool IsValid { get; set; }
        public ValidationType Type { get; set; }
        public string Message { get; set; }
        public object Response { get; set; }
    }

    public class RequiredAttribute : Attribute
    {
    }

    public enum ValidationType
    {
        None,
        FormValidation,
        RemoteServerValidation,
        Exception,
        NoResults
    }
}
