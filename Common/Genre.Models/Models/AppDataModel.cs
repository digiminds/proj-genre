﻿using System.Collections.Generic;
using Xamarin.Forms;

namespace Genre.Models.Models
{
    public class AppDataModel
    {
        public int Code { get; set; }

        public string CodeTitle => $"Your code is: {Code}";

        public ApplicantInfo ApplicantInfo { get; set; }

        public IEnumerable<ApplicationInfo> Applications{ get; set; }

        public AppDataModel()
        {
            ApplicantInfo = new ApplicantInfo();
        }
    }

    public class ApplicantInfo
    {
        public Gender Gender { get; set; }
    }

    public class ApplicationInfo
    {
        public string Code { get; set; }
        public string Title { get; set; }
        public string PieceImage => Rated ? PieceImageOn : PieceImageOff;
        public string PieceImageOn { get; set; }
        public string PieceImageOff { get; set; }
        public Rectangle Rectangle { get; set; }
        public bool Rated { get; set; }
    }

    public enum Gender
    {
        Male,
        Female
    }
}
