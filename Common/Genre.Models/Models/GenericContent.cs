﻿using System.Collections.Generic;

namespace Genre.Models.Models
{
    public class GenericContent
    {
        public string Name { get; set; }
        public string Title { get; set; }
        public string Language { get; set; }
        public List<object> Values { get; set; }
        public List<GenericContent> Contents { get; set; }
    }
}
