﻿using Genre.Models.Models;

namespace Genre.Models.Abstractions
{
    public interface IFormModel
    {
        ValidationResult IsValid();
    }
}
