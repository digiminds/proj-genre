﻿using System;
using Xamarin.Forms;

namespace Genre.Models.Abstractions
{
    public interface IFormInput
    {
        bool Invalid { get; set; }
        string ValidationMessage { get; set; }
        Func<Color, bool> SetLineColor { get; set; }

        bool IsValid();
        void ShowError(string message);
        void RemoveError();
    }
}
