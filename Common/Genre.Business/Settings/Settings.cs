﻿using Plugin.Settings;
using Plugin.Settings.Abstractions;

namespace Genre.Business.Settings
{
    public static class Settings
    {
        private static ISettings AppSettings => CrossSettings.Current;

        private const string ApplicantCodeKey = "ApplicantCode_Key";
        public static int ApplicantCode
        {
            get => AppSettings.GetValueOrDefault(ApplicantCodeKey, 0);
            set => AppSettings.AddOrUpdateValue(ApplicantCodeKey, value);
        }
    }
}
