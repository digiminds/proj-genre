﻿using System;
using Newtonsoft.Json;

namespace Genre.Business.Mappers
{
    public class ResponseMapper
    {
    }

    public static class Deserializer<T>
    {
        public static T Deserialize(string value)
        {
            if (string.IsNullOrEmpty(value)) return default(T);

            T result = default(T);
            try
            {
                result = JsonConvert.DeserializeObject<T>(value);
            }
            catch (Exception)
            {
                return result;
            }

            return result;
        }
    }
}
