﻿using Genre.Business.Data;
using Genre.Models.Abstractions;
using Xamarin.Forms;

[assembly: Dependency(typeof(NetworkDataStore))]
namespace Genre.Business.Data
{
    public class NetworkDataStore : IDataStore
    {
        private readonly object Lock = new object();

        public NetworkDataStore()
        {
        }
    }
}
