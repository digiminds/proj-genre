﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using Xamarin.Forms;

namespace Genre.Business.Utils
{
    public static class Extensions
    {
        private static readonly Regex SInternalName = new Regex("[^a-zA-Z0-9]");
        
        public static string SanitizePhoneNumber(this string value)
        {
            return new string(value.ToCharArray().Where(char.IsDigit).ToArray());
        }

        public static IEnumerable<T> Clone<T>(this IEnumerable<T> listToClone) where T : ICloneable
        {
            return listToClone.Select(item => (T)item.Clone()).ToList();
        }

        public static string ToInternalName(this string str)
        {
            return SInternalName.Replace(str, "").ToLower();
        }

        public static async Task PushPage(this INavigation navigation, Page page, bool animation = false)
        {
            await navigation.PushAsync(page, animation);
        }

        public static async Task PopPage(this INavigation navigation, bool animation = false)
        {
            await navigation.PopAsync(animation);
        }
    }
}
