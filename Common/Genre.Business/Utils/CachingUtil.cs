﻿using System;
using System.Threading.Tasks;
using Microsoft.Extensions.Caching.Memory;

namespace Genre.Business.Utils
{
    public static class CachingUtil
    {
        private static IMemoryCache _cache;
        private static readonly object Lock = new object();

        private static bool UseMemoryCache => true;

        private static IMemoryCache Cache
        {
            get
            {
                if (_cache == null)
                {
                    lock (Lock)
                    {
                        _cache = new MemoryCache(new MemoryCacheOptions
                        {
                            ExpirationScanFrequency = new TimeSpan(TimeSpan.TicksPerMinute * 15)
                        });
                    }
                }

                return _cache;
            }
        }

        public static async Task<T> GetOrCompute<T>(string key, Func<Task<T>> computeMethod)
            where T : class
        {
            T result = null;
            if (UseMemoryCache)
            {
                result = Cache.Get(key) as T;
            }
            if (result == null)
            {
                result = await computeMethod();
                if (result != null && UseMemoryCache)
                {
                    Cache.Set(key, result, DateTimeOffset.Now.AddMinutes(5.0));
                }
            }

            if (result is ICloneable clonableResult)
            {
                result = clonableResult.Clone() as T;
            }
            return result;
        }

        public static T GetOrCompute<T>(string key, Func<T> computeMethod)
            where T : class
        {
            T result = null;
            if (UseMemoryCache)
            {
                result = Cache.Get(key) as T;
            }
            if (result == null)
            {
                result = computeMethod();
                if (result != null && UseMemoryCache)
                {
                    Cache.Set(key, result, DateTimeOffset.Now.AddMinutes(5.0));
                }
            }

            if (result is ICloneable clonableResult)
            {
                result = clonableResult.Clone() as T;
            }
            return result;
        }
    }
}
