﻿using System;
using System.Globalization;
using Xamarin.Forms;

namespace Genre.Business.Utils
{
    public class ColorConverter : IValueConverter
    {
        #region IValueConverter implementation
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            var colorValue = (string)value;
            return Color.FromHex(colorValue);
        }
        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            var colorValue = (string)value;
            return Color.FromHex(colorValue);
        }
        #endregion

    }

    public class BooleanInverter : IValueConverter
    {
        #region IValueConverter implementation
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            return !(bool)value;
        }
        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            return !(bool)value;
        }
        #endregion

    }
}
