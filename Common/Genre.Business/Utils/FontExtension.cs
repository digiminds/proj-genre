﻿using System;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace Genre.Business.Utils
{
    [ContentProperty("Value")]
    public class FontExtension : IMarkupExtension
    {
        public string Value { get; set; }

        public FontExtension()
        {

        }

        public object ProvideValue(IServiceProvider serviceProvider)
        {
            switch (Value)
            {
                case "ObjectiveBold":
                    return Device.RuntimePlatform == Device.Android
                        ? "Objective-Bold.otf#Objective-Bold"
                        : "Objective-Bold";
                case "ObjectiveMedium":
                    return Device.RuntimePlatform == Device.Android
                        ? "Objective-Medium.otf#Objective-Medium"
                        : "Objective-Medium";
                case "ObjectiveRegular":
                    return Device.RuntimePlatform == Device.Android
                        ? "Objective-Regular.otf#Objective-Regular"
                        : "Objective-Regular";
                default:
                    return Device.RuntimePlatform == Device.Android
                        ? "Objective-Regular.otf#Objective-Regular"
                        : "Objective-Regular";
            }
        }
    }
}
